#!/bin/bash

USER=$(whoami)
echo "Installing startup script"

cat <<EOF > $HOME/.startup.sh
#!/bin/bash
rm -rf $HOME/.mozilla/firefox
EOF

chmod +x $HOME/.startup.sh

echo "Installing systemd service"
cat <<EOF > $HOME/.startup.service
[Unit]
Description=Somos Acogida startup script
DefaultDependencies=no
After=network.target

[Service]
Type=simple
User=$(whoami)
ExecStart=$HOME/.startup.sh
TimeoutStartSec=0
RemainAfterExit=yes

[Install]
WantedBy=default.target
EOF

cat $HOME/.startup.service | sudo tee /etc/systemd/system/somos-acogida.service
echo "Enabling systemd service"
sudo systemctl enable somos-acogida
echo "Restart now"
